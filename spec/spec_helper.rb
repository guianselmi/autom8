# frozen_string_literal: true

require 'pry'
require 'watir/rspec'
require 'webdrivers'

RSpec.configure do |config|
  # Use Watir::RSpec::HtmlFormatter to get links to the screenshots, html and
  # all other files created during the failing examples.
  config.add_formatter(:progress) if config.formatters.empty?
  config.add_formatter(Watir::RSpec::HtmlFormatter)

  # Open up the browser for each example.
  options = {}
  options[:args] = ['--window-size=1920,1080']
  options[:args] << '--headless' if ENV['DRIVER'] == 'headless'

  config.before :all do
    @browser = Watir::Browser.new :chrome, options: options
  end

  # Close that browser after each example.
  config.after :all do
    @browser&.close
  end

  # Include RSpec::Helper into each of your example group for making it possible to
  # write in your examples instead of:
  #   @browser.goto "localhost"
  #   @browser.text_field(name: "first_name").set "Bob"
  #
  # like this:
  #   goto "localhost"
  #   text_field(name: "first_name").set "Bob"
  #
  # This needs that you've used @browser as an instance variable name in
  # before :all block.
  config.include Watir::RSpec::Helper

  # Include RSpec::Matchers into each of your example group for making it possible to
  # use #within with some of RSpec matchers for easier asynchronous testing:
  #   expect(@browser.text_field(name: "first_name")).to exist.within(2)
  #   expect(@browser.text_field(name: "first_name")).to be_present.within(2)
  #   expect(@browser.text_field(name: "first_name")).to be_visible.within(2)
  #
  # You can also use #during to test if something stays the same during the specified period:
  #   expect(@browser.text_field(name: "first_name")).to exist.during(2)
  config.include Watir::RSpec::Matchers

  # https://relishapp.com/rspec/rspec-mocks/v/3-10/docs/verifying-doubles/partial-doubles
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
end
