# autom8

🤖 Test Automation Framework playground.

Create the development environment by starting Vagrant while using this [`Vagrantfile`](/vagrant_autom8/Vagrantfile). It automatically provisions a machine consisting of:
* Ubuntu 20.04
* Ruby 3.0.2
* Chrome and Firefox

## Instructions

To start and access the dev environment, clone this repo, and then:
```bash
# Start the environment
cd vagrant_autom8
vagrant up
vagrant ssh

cd autom8 # Go to the automation project

# Run the tests! :)
```

Note: the test data is currently WIP.

For more information on how to install and run Vagrant, please check the [offical page](https://learn.hashicorp.com/collections/vagrant/getting-started).
