 # Use custom tmux config file
 cp /vagrant/.tmux.conf ~

 # Add aliases to .bashrc
 echo "alias bi='bundle install'" >> ~/.bashrc
 echo "alias be='bundle exec'" >> ~/.bashrc
 echo "alias ber='be rspec'" >> ~/.bashrc
 echo "alias hber='DRIVER=headless be rspec'" >> ~/.bashrc

 # Install rbenv
 git clone https://github.com/rbenv/rbenv.git ~/.rbenv
 echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
 echo 'eval "$(rbenv init -)"' >> ~/.bashrc

 # Install ruby-build
 git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
 echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc

 export PATH="$HOME/.rbenv/bin:$PATH"
 export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
 eval "$(rbenv init -)"

 # Install ruby and make it the global version
 rbenv install 3.0.2
 rbenv global 3.0.2

 # Install bundler
 gem install bundler

 # Rehash
 rbenv rehash

 # Run `bundle install` to install the dependencies
 cd autom8
 bundle install
