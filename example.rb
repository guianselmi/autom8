# frozen_string_literal: true

require 'spec_helper'

describe 'Google' do
  before { goto 'http://google.com' }

  it 'has search box' do
    button(text: 'Ich stimme zu').click
    expect(text_field(name: 'q')).to be_present
  end

  it 'allows to search' do
    text_field(name: 'q').set 'watir'
    expect(1).to eq 1
  end
end
