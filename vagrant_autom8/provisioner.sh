# Add Chrome repository
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c "echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/google.list"

# Update and Upgrade
apt update
apt upgrade -y

# Install browsers
apt install -y google-chrome-stable chromium-browser firefox

# Install other applications and dependencies
apt install -y x11-apps libssl-dev zlib1g-dev neovim
